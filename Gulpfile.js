// JavaScript Document

/* ************************************************************************************************************************

Laboratorio Nomada Medial

File:			Gulpfile.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');

// CSS

gulp.task('css', function () {
	return gulp
		.src('wp-content/themes/Laboratorio-Nomada-Medial/assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('wp-content/themes/Laboratorio-Nomada-Medial/build'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('wp-content/themes/Laboratorio-Nomada-Medial/assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('wp-content/themes/Laboratorio-Nomada-Medial/build'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('wp-content/themes/Laboratorio-Nomada-Medial/assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('wp-content/themes/Laboratorio-Nomada-Medial/build'));
});

// Watch

gulp.task('watch', function () {
	gulp.watch(['wp-content/themes/Laboratorio-Nomada-Medial/assets/css/**/*.scss'], ['css']);
	gulp.watch(['wp-content/themes/Laboratorio-Nomada-Medial/assets/js/**/*.js'], ['js']);
});

// Default

gulp.task('default', ['css', 'js', 'image', 'watch']);